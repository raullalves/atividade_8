#include "stdafx.h"
int HeuristicSolver::Solve(int method, Data* data)
{
	switch (method)
	{
	case GREEDY:
		return SolveGreedy(data);
	case STINGY:
			return SolveStingy(data);
	default:
		return SolveAlternate(data);
	}
}

int HeuristicSolver::SolveGreedy(Data* data)
{
	return std::get<0>(SolveGreedyWrapper(data));
}

int HeuristicSolver::SolveStingy(Data* data)
{
	std::vector<int> facilities;
	std::vector<int> whereClientsAreAlocated;

	int min_cost = 0;

	//initialization
	for (int client = 0; client < data->GetQtdLocations(); client++)
	{
		auto facility = GetBestFacilityPerClient(data, client);
		whereClientsAreAlocated.push_back(std::get<0>(facility));
		min_cost += std::get<1>(facility);
		if (std::find(facilities.begin(), facilities.end(), std::get<0>(facility)) == facilities.end()) facilities.push_back(std::get<0>(facility));
	}

	std::vector<int> whereClientsAreAlocatedTemp;
	std::copy(whereClientsAreAlocated.begin(), whereClientsAreAlocated.end(), std::back_inserter(whereClientsAreAlocatedTemp));

	int actual_cost = min_cost;
	while (facilities.size() != data->GetQtdMedians())
	{
		std::vector<int> whereClientsAreAlocatedTemp;
		std::copy(whereClientsAreAlocated.begin(), whereClientsAreAlocated.end(), std::back_inserter(whereClientsAreAlocatedTemp));

		int min_facility = -1;
		int diff = 999999;

		for (const auto& facility_to_remove : facilities)
		{
			std::vector<int> whereClientsAreAlocatedTemp_per_facility;
			std::copy(whereClientsAreAlocated.begin(), whereClientsAreAlocated.end(), std::back_inserter(whereClientsAreAlocatedTemp_per_facility));

			int sum = 0;
			for (int client = 0; client < data->GetQtdLocations(); client++)
			{
				int min_facility_per_client = whereClientsAreAlocatedTemp.at(client);
				int min_cost_per_client =999999;

				if (min_facility_per_client != facility_to_remove)
				{
					sum+= (data->GetCosts())[client][min_facility_per_client];
					continue;
				}
				std::vector<int> facilities_temp;
				std::copy(facilities.begin(), facilities.end(), std::back_inserter(facilities_temp));

				auto pos = std::find(facilities_temp.begin(), facilities_temp.end(), min_facility_per_client);
				facilities_temp.erase(pos);

				for (const auto& facility : facilities_temp)
				{
					if (facility == min_facility_per_client) continue;

					if ((data->GetCosts())[client][facility] < min_cost_per_client)
					{
						min_cost_per_client = (data->GetCosts())[client][facility];
						min_facility_per_client = facility;
					}
				}
				sum += min_cost_per_client;
				whereClientsAreAlocatedTemp_per_facility.at(client) = min_facility_per_client;
			}
			if (sum - actual_cost < diff)
			{
				min_facility = facility_to_remove;
				diff = sum - actual_cost;
				actual_cost = sum;
				whereClientsAreAlocatedTemp.clear();
				std::copy(whereClientsAreAlocatedTemp_per_facility.begin(), whereClientsAreAlocatedTemp_per_facility.end(), std::back_inserter(whereClientsAreAlocatedTemp));
			}
		}

		auto pos = std::find(facilities.begin(), facilities.end(), min_facility);
		facilities.erase(pos);
		min_cost = actual_cost;
		whereClientsAreAlocated.clear();
		std::copy(whereClientsAreAlocatedTemp.begin(), whereClientsAreAlocatedTemp.end(), std::back_inserter(whereClientsAreAlocated));
	}

	return min_cost;
}

int HeuristicSolver::SolveAlternate(Data* data)
{
	std::map<int, std::vector<int>> whichClientsBelongToWitchFacility;
	std::vector<int> whereClientsAreAlocated;
	std::vector<int> randomP;
	for (int i = 0; i < data->GetQtdMedians(); i++)
	{
		int n = Utils::GenerateRandomNumber(0, data->GetQtdLocations());
		while (std::find(randomP.begin(), randomP.end(), n) != randomP.end())
		{
			n = Utils::GenerateRandomNumber(0, data->GetQtdLocations());
		}
		randomP.push_back(n);
	}

	for (int client = 0; client < data->GetQtdLocations(); client++)
	{
		int pos = Utils::GenerateRandomNumber(0, data->GetQtdMedians()-1);
		whereClientsAreAlocated.push_back(randomP.at(pos));
	}

	std::vector<int> facilities;

	for (int facility = 0; facility < data->GetQtdLocations(); facility++)
	{
		std::vector<int> clientsAlocatedAtFacility_i;
		
		for (int i = 0; i < whereClientsAreAlocated.size(); i++)
		{
			if (whereClientsAreAlocated.at(i) == facility)
			{
				clientsAlocatedAtFacility_i.push_back(i);
			}
		}
		
		if (clientsAlocatedAtFacility_i.size() > 0)
		{
			whichClientsBelongToWitchFacility[facility] = clientsAlocatedAtFacility_i;
			facilities.push_back(facility);
		}
	}

	whereClientsAreAlocated.clear();
	bool isMoving = true;

	int sum = 0;
	for (int client = 0; client < data->GetQtdLocations(); client++)
	{
		int min_facility = facilities.at(0);
		int min_cost_per_client = (data->GetCosts())[client][min_facility];
		for (const auto& facility : facilities)
		{
			if ((data->GetCosts())[client][facility] < min_cost_per_client)
			{
				min_cost_per_client = (data->GetCosts())[client][facility];
				min_facility = facility;
			}
		}
		sum += min_cost_per_client;
		whereClientsAreAlocated.push_back(min_facility);
	}

	whichClientsBelongToWitchFacility.clear();
	for (int facility = 0; facility < data->GetQtdLocations(); facility++)
	{
		std::vector<int> clientsAlocatedAtFacility_i;
		for (int i = 0; i < whereClientsAreAlocated.size(); i++)
		{
			if (whereClientsAreAlocated.at(i) == facility)
			{
				clientsAlocatedAtFacility_i.push_back(i);
			}
		}
		if (clientsAlocatedAtFacility_i.size() > 0)
		{
			whichClientsBelongToWitchFacility[facility] = clientsAlocatedAtFacility_i;
			facilities.push_back(facility);
		}
	}

	int min_cost = sum;

	while (isMoving)
	{
		std::map<int, std::vector<int>> newWhichClientsBelongToWitchFacility(whichClientsBelongToWitchFacility);
		
		isMoving = false;
		int min_cost_temp = 0;
		for (auto info : whichClientsBelongToWitchFacility)
		{
			int initial_facility = info.first;
			auto clients = info.second;
			int total_cost_of_facility = 9999999;
			int min_facility = -1;
			for (const auto& facility : clients)
			{
				int sum_all_clients_at_facility = 0;
				for (const auto& client : clients)
				{
					sum_all_clients_at_facility += (data->GetCosts())[client][facility];
				}

				if (sum_all_clients_at_facility < total_cost_of_facility)
				{
					total_cost_of_facility = sum_all_clients_at_facility;
					min_facility = facility;
				}
			}
			min_cost_temp += total_cost_of_facility;
			if (min_facility != initial_facility)
			{
				auto nodeHandler = newWhichClientsBelongToWitchFacility.extract(initial_facility);
				nodeHandler.key() = min_facility;
				newWhichClientsBelongToWitchFacility.insert(std::move(nodeHandler));
				isMoving = true;
			}
		}

		min_cost = min_cost_temp;
		
		whichClientsBelongToWitchFacility.clear();
		whichClientsBelongToWitchFacility = newWhichClientsBelongToWitchFacility;
	}
	
	return min_cost;
}

std::tuple<int, std::vector<int>> HeuristicSolver::SolveGreedyWrapper(Data* data)
{
	auto oneMedian = Solve1Median(data);

	std::vector<int> facilities;

	facilities.push_back(std::get<0>(oneMedian));

	std::vector<int> whereClientsAreAlocated;
	for (int client = 0; client < data->GetQtdLocations(); client++) whereClientsAreAlocated.push_back(std::get<0>(oneMedian));

	int min_cost = std::get<1>(oneMedian);

	for (int p = 1; p < data->GetQtdMedians(); p++)
	{
		std::vector<int> whereClientsAreAlocatedTemp;
		std::copy(whereClientsAreAlocated.begin(), whereClientsAreAlocated.end(), std::back_inserter(whereClientsAreAlocatedTemp));

		int new_facility = 0;

		for (int facility = 0; facility < data->GetQtdLocations(); facility++)
		{
			if (std::find(facilities.begin(), facilities.end(), facility) != facilities.end()) continue;

			std::vector<int> whereClientsAreAlocatedTemp_per_facility;
			std::copy(whereClientsAreAlocated.begin(), whereClientsAreAlocated.end(), std::back_inserter(whereClientsAreAlocatedTemp_per_facility));

			int sum = 0;
			for (int client = 0; client < data->GetQtdLocations(); client++)
			{
				int min_cost_per_client = (data->GetCosts())[client][facility];
				int min_facility_per_client = facility;
				
				for (const auto& old_facility : facilities)
				{
					if ((data->GetCosts())[client][old_facility] < min_cost_per_client)
					{
						min_cost_per_client = (data->GetCosts())[client][old_facility];
						min_facility_per_client = old_facility;
					}
				}

				sum += (data->GetCosts())[client][min_facility_per_client];
				whereClientsAreAlocatedTemp_per_facility.at(client) = min_facility_per_client;
			}

			if (sum < min_cost)
			{
				new_facility = facility;
				min_cost = sum;
				whereClientsAreAlocatedTemp.clear();
				std::copy(whereClientsAreAlocatedTemp_per_facility.begin(), whereClientsAreAlocatedTemp_per_facility.end(), std::back_inserter(whereClientsAreAlocatedTemp));
			}
		}

		facilities.push_back(new_facility);
		whereClientsAreAlocated.clear();
		std::copy(whereClientsAreAlocatedTemp.begin(), whereClientsAreAlocatedTemp.end(), std::back_inserter(whereClientsAreAlocated));
	}
	
	return std::make_tuple(min_cost, whereClientsAreAlocated);
}

std::tuple<int, int> HeuristicSolver::GetBestFacilityPerClient(Data* data, int client)
{
	int min_cost = 99999999;
	int min_facility = -1;
	for (int facility = 0; facility < data->GetQtdLocations(); facility++)
	{
		if ((data->GetCosts())[client][facility] < min_cost)
		{
			min_cost = (data->GetCosts())[client][facility];
			min_facility = facility;
		}
	}
	return std::make_tuple(min_facility, min_cost);
}

std::tuple<int,int> HeuristicSolver::Solve1Median(Data* data)
{
	int min_cost = 99999999;
	int min_facility = -1;

	for (int facility = 0; facility < data->GetQtdLocations(); facility++)
	{
		int sum = 0;
		for (int client = 0; client < data->GetQtdLocations(); client++)
		{
			sum += (data->GetCosts())[client][facility];
		}

		if (sum < min_cost)
		{
			min_cost = sum;
			min_facility = facility;
		}
	}

	return std::make_tuple(min_facility, min_cost);
}