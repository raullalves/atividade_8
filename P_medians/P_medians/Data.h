#pragma once
class Data
{
public:
	Data(std::string addr)
	{
		this->addr = "data/" + addr + ".pmp";
	}

	virtual ~Data() 
	{ 
		for (int i = 0; i < this->numberOfLocations; i++)
		{
			delete cost[i];
		}
		delete[]cost; 
	}

	void CreateCost()
	{
		cost = new int* [this->numberOfLocations];
		for (int i = 0; i < this->numberOfLocations; i++)
		{
			cost[i] = new int[this->numberOfLocations];
		}
	}

	void AssignCost(int i, int j, int value)
	{
		cost[i][j] = value;
	}

	int** GetCosts() { return this->cost; }

	int LoadData();

	int GetQtdLocations() { return this->numberOfLocations; }
	int GetQtdMedians() { return this->numberOfMedians; }
	void GetCost(int**& cost2) { cost2 = this->cost; }
private:
	std::string addr = "";
	int numberOfLocations;
	int numberOfMedians;

	int **cost = 0;
};