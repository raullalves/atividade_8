#include "stdafx.h"

int main()
{
	std::list<std::string> instances = { "instance0","instance1","instance2","instance3",
										"instance4","instance5","instance6","instance7",
										"instance8","instance9" };

	std::list<int> methods = { GREEDY, STINGY, ALTERNATE };

	for (const auto& method : methods)
	{
		std::cout << "Going to run method " << method << std::endl;
		for (const auto& instance : instances)
		{
			auto logger = new Logger(instance, method);

			auto data = new Data(instance);
			if (data->LoadData() != OK)
			{
				logger->Print("Failed to load file");
				return ERROR;
			}
			auto res = HeuristicSolver::Solve(method, data);

			logger->Print(std::to_string(res));
			delete logger;
			delete data;
		}
	}
	return OK;
}