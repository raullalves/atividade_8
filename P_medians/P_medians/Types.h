#pragma once

static const int OK = 0;
static const int ERROR = 1;

static const int MAX = 1000;

static const int GREEDY = 1;
static const int STINGY = 2;
static const int ALTERNATE = 3;