#pragma once
#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <fstream>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <sstream>
#include <map>
#include <algorithm>
#include <iterator>
#include <atomic>
#include <random>

#include "gurobi_c++.h"

#include "Types.h"
#include "Utils.h"
#include "Logger.h"
#include "Data.h"
#include "HeuristicSolver.h"