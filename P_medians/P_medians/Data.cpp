#include "stdafx.h"
int Data::LoadData()
{
	std::fstream file;
	file.open(this->addr, std::ios::in);
	if (!(file.is_open() && file.good())) return ERROR;

	int temp_cost;

	file >> this->numberOfLocations;
	file >> this->numberOfMedians;

	CreateCost();

	int i = 0;
	while (!file.eof())
	{
		for (int j = 0; j < this->numberOfLocations; j++)
		{
			file >> temp_cost;
			AssignCost(i, j, temp_cost);
		}

		i++;
	}

	return OK;
}