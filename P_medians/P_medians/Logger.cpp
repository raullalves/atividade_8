#include "stdafx.h"

void Logger::Print(std::string msg)
{
	std::ofstream log(this->filename, std::ios_base::app | std::ios_base::out);

	log << msg << "\n";

	std::cout << msg << std::endl;
}