#pragma once
class HeuristicSolver
{
public:
	static int Solve(int, Data*);
private:
	static int SolveGreedy(Data*);
	static int SolveStingy(Data*);
	static int SolveAlternate(Data*);
	static std::tuple<int, std::vector<int>> SolveGreedyWrapper(Data*);
	static std::tuple<int, int> GetBestFacilityPerClient(Data*, int);
	static std::tuple<int,int> Solve1Median(Data*);
};