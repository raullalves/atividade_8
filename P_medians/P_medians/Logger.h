#pragma once
class Logger
{
public:
	Logger(std::string filename, int method) { this->filename = "logs/" + filename + "_m=" + Utils::int2MethodName(method) + ".txt"; }

	void Print(std::string);
private:
	std::string filename = "";
};