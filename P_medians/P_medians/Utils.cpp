#include "stdafx.h"
std::string Utils::int2MethodName(int i)
{
	switch (i)
	{
	case GREEDY:
		return "GREEDY";
	case STINGY:
		return "STINGY";
	default:
		return "ALTERNATE";
	}
}
int Utils::GenerateRandomNumber(int from, int to)
{
	if (from >= to) return from;

	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::uniform_int_distribution<> range(from, to);

	return range(generator);
}